
import regression from './regression';

// let points = [ [1, 1], [2, 3] ];
// let result = regression.linear(points);
// console.log(result.string);

function smoothing(size) {

    let counter = 0;
    let history = [];

    return function (value) {

        let index = counter++;

        history.push([index, value]);
        while (history.length > size) {
            history.shift();
        }

        let result = regression.linear(history);

        //console.log(history, result.string);

        return result.equation[0] * index + result.equation[1];
    }

}

export default smoothing

